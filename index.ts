/**
 * Recursively parses stringified data until it is no longer a string
 * @param data - Stringified data to be pased
 * @returns A parsed copy of the data or an error
 */
function unstringify(data: string): any {
  try {
    const parsed = JSON.parse(data);

    if (typeof parsed === 'string') {
      return unstringify(parsed);
    }

    return parsed;

  } catch (error) {
    return error;
  }
}

module.exports = unstringify;
